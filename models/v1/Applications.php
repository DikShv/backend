<?php

namespace app\models\v1;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property int|null $date_application 
 * @property int|null $date_shipping
 * @property int|null $date_deferral
 * @property int|null $day_deferral
 * @property float|null $sum
 * @property int|null $status
 * @property int|null $user_id
 *
 * @property ApplicationItems[] $applicationItems
 * @property Users $user
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

   
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_application'], 'required'],
            [['date_application', 'date_shipping', 'date_deferral', 'day_deferral', 'status', 'user_id'], 'default', 'value' => null],
            [['date_application', 'date_shipping', 'date_deferral', 'date_payment'], 'date', 'format' => 'php:Y-m-d'],
            [['day_deferral', 'status', 'user_id'], 'integer'],
            [['sum'], 'number'],
            [['comment'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_application' => 'Date Application',
            'date_shipping' => 'Date Shipping',
            'date_deferral' => 'Date Deferral',
            'day_deferral' => 'Day Deferral',
            'sum' => 'Sum',
            'status' => 'Status',
            'user_id' => 'User ID',
            'comment' => 'Comment',
        ];
    }

    /**
     * Gets query for [[ApplicationItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplicationItems()
    {
        return $this->hasMany(ApplicationItems::className(), ['application_id' => 'id']);
    }

   
    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_application = strtotime($this->date_application);
            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {   
        if(!is_null($this->date_application))
            $this->date_application = Yii::$app->formatter->asDate($this->date_application);
        if(!is_null($this->date_shipping))
            $this->date_shipping = Yii::$app->formatter->asDate($this->date_shipping);
        if(!is_null($this->date_deferral))
            $this->date_deferral = Yii::$app->formatter->asDate($this->date_deferral);
        if(!is_null($this->date_payment))
            $this->date_payment = Yii::$app->formatter->asDate($this->date_payment);

    }
    
}
