<?php

namespace app\models\v1;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $email
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /*public $auth_key;
    public $id;
    public $username;
    public $password;
    public $password_hash;
    public $expire_at;
    public $access_token;*/


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'default', 'value' => null],
            [['id'], 'integer'],
            [['email'], 'string', 'max' => 25],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'company' => "Компания"
        ];
    }

    public function generateAccessToken()
    {
        $this->access_token=Yii::$app->security->generateRandomString();
        return $this->access_token;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {   
        $user = static::findOne(['access_token' => $token]);
        if (!$user) {
            return false;
        }
        if ($user->expire_at < time()) {
            throw new UnauthorizedHttpException('the access - token expired ', -1);
        } else {
            return $user;
        }
    }

    public static function findByUsername($username)
    {
        $user = static::findOne(['username' => $username]);
        if ($user) {
            return $user;
        }

        return null;
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }    

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
}
