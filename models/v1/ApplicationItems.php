<?php

namespace app\models\v1;

use Yii;

/**
 * This is the model class for table "application_items".
 *
 * @property int $id
 * @property int|null $specific_id
 * @property int|null $application_id
 * @property int|null $count
 *
 * @property Applications $application
 * @property Specifics $specific
 */
class ApplicationItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['specific_id', 'application_id', 'count'], 'default', 'value' => null],
            [['specific_id', 'application_id', 'count'], 'integer'],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => Applications::className(), 'targetAttribute' => ['application_id' => 'id']],
            [['specific_id'], 'exist', 'skipOnError' => true, 'targetClass' => Specifics::className(), 'targetAttribute' => ['specific_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'specific_id' => 'Specific ID',
            'application_id' => 'Application ID',
            'count' => 'Count',
        ];
    }

    /**
     * Gets query for [[Application]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        
        return $this->hasOne(Applications::className(), ['id' => 'application_id']);
    }

    /**
     * Gets query for [[Specific]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpecific()
    {   //print_r($this);exit();
        return $this->hasOne(Specifics::className(), ['id' => 'specific_id']);
    }
}
