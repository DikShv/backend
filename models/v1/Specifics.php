<?php

namespace app\models\v1;

use Yii;

/**
 * This is the model class for table "specifics".
 *
 * @property int $id
 * @property string|null $articul
 * @property string|null $name
 * @property string|null $measure
 * @property float|null $price
 * @property int|null $stock_balance 
 */
class Specifics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'specifics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'default', 'value' => null],
            [['id'], 'integer'],
            [['price'], 'number'],
            [['stock_balance '], 'default', 'value' => null],
            [['stock_balance '], 'integer'],
            [['articul', 'measure'], 'string', 'max' => 20],
            [['name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'articul' => 'Articul',
            'name' => 'Name',
            'measure' => 'Measure',
            'price' => 'Price',
            'stock_balance ' => 'Stock Balance',
        ];
    }

    public function afterFind()
    {   
        $url = \Yii::$app->request->hostInfo;
        if(!is_null($this->photo))
            $this->photo = $url."/api/images/".$this->photo;

    }
}
