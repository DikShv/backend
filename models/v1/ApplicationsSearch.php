<?php

namespace app\models\v1;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\v1\Applications;
use app\models\v1\ApplicationItems;
use app\models\v1\Users;
use Yii;

/**
 * ApplicationsSearch represents the model behind the search form of `app\models\v1\Applications`.
 */
class ApplicationsSearch extends Applications
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['date_application', 'date_payment'], 'date', 'format' => 'php:Y-m-d']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Applications::find()->alias('a')
        ->leftJoin(ApplicationItems::tableName().' ai', 'a.id=ai.application_id')
        ->select('a.*, SUM(ai.sum) as sum')
        ->groupBy('a.id')
        ->where(['a.user_id'=>Yii::$app->user->id]);

        $page=(!empty($params['page'])?intval($params['page']):1);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => 20,
                'page'=> ($page<1?1:$page-1)
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->filterWhere([
            'user_id' => Yii::$app->user->id,
        ]);
        if(!empty($this->date_application)){
            $timestamp_start = strtotime($this->date_application);
            $timestamp_end = strtotime($this->date_application."+1 days");
            $query->andFilterWhere(['>=', 'date_application', $timestamp_start ]);
            $query->andFilterWhere(['<', 'date_application', $timestamp_end ]);
        } 
        if(!empty($this->date_payment)){
            $timestamp_start = strtotime($this->date_payment);
            $timestamp_end = strtotime($this->date_payment."+1 days");
            $query->andFilterWhere(['>=', 'date_payment', $timestamp_start ]);
            $query->andFilterWhere(['<', 'date_payment', $timestamp_end ]);
        } 
        if(!empty($this->status)){
            $query->andFilterWhere([
                'status' => $this->status,
            ]);
        }
        $count=$query->count();
        $pages=$count>20?ceil($count / 20):1;
        $result=['count'=>$count,'pages'=>$pages,'page'=>$page,'items'=>$dataProvider];
        return $result;
    }

    /*Поиск для Push*/
    public function search_for_push($params)
    {
        $query = Applications::find()->alias('a')
        ->leftJoin(ApplicationItems::tableName().' ai', 'a.id=ai.application_id')
        ->leftJoin(Users::tableName().' u', 'a.user_id=u.id')
        ->select('a.*, SUM(ai.sum) as sum, u.push_id')
        ->asArray()
        ->groupBy('a.id, u.id');

        $this->load($params);

        if(!empty($this->date_application)){
            $timestamp_start = strtotime($this->date_application);
            $timestamp_end = strtotime($this->date_application."+1 days");
            $query->andFilterWhere(['>=', 'date_application', $timestamp_start ]);
            $query->andFilterWhere(['<', 'date_application', $timestamp_end ]);
        } 
        if(!empty($this->date_payment)){
            $timestamp_start = strtotime($this->date_payment);
            $timestamp_end = strtotime($this->date_payment."+1 days");
            $query->andFilterWhere(['>=', 'date_payment', $timestamp_start ]);
            $query->andFilterWhere(['<', 'date_payment', $timestamp_end ]);
        } 
        if(!empty($this->status)){
            $query->andFilterWhere([
                'status' => $this->status,
            ]);
        }
        return $query->all();
    }
}
