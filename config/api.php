<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'homeUrl' => '/api',
    'name' => 'Премиум сервис',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@api' => dirname(dirname(__DIR__)) . '/api',
    ],
    'language' => 'ru-RU',
    'timeZone' => 'Asia/Yekaterinburg',
    'components' => [
        'request' => [
        	'BaseUrl' => '/api',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            //'enableCookieValidation' => false,
            'cookieValidationKey' => 'YiDQybEclHqcJK25QOZ-8aOAEU3wOkwY',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\v1\Users',
            'enableAutoLogin' => true,
            'enableSession' => false,
            'loginUrl' => null,
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'RUB',
       ],
       /* 'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],*/
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['v1/infos'],
                    'extraPatterns'=>[
                        '<id:\w+>'=>'view',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['v1/users'],
                    'extraPatterns'=>[
                        'GET test'=>'test',
                        'POST login'=>'login',
                    ],
                ],
                
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['v1/specifics'],
                ],
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['v1/applications'],
                    /*'extraPatterns'=>[
                        'GET index'=>'index',
                    ],*/
                ],
                
                
            ],
           
        ]
    ],
    'modules' => [
        'v1' => [
            'class' => 'app\modules\v1\Module',
            'basePath' => '@app/modules/v1',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
