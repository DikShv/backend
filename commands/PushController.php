<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\v1\Users;
use app\modules\admin\models\Info;
use app\models\v1\ApplicationsSearch;

class PushController extends Controller
{
    
    public function actionIndex()
    {
        $date=date('Y-m-d');
        $available_from_date_start  = strtotime($date." +3 days");
        $available_from_date_end  = strtotime($date." +4 days");
        echo $available_from_date_start . " - ".date('Y-m-d H:i:s', $available_from_date_start)."\n";
        echo $available_from_date_end . " - ".date('Y-m-d H:i:s', $available_from_date_end)."\n";
        $available_from_date_start  = strtotime($date." +1 days");
        $available_from_date_end  = strtotime($date." +2 days");
        echo $available_from_date_start . " - ".date('Y-m-d H:i:s', $available_from_date_start)."\n";
        echo $available_from_date_end . " - ".date('Y-m-d H:i:s', $available_from_date_end)."\n";
        $available_from_date_start  = strtotime($date);
        $available_from_date_end  = strtotime($date." +1 days");
        echo $available_from_date_start . " - ".date('Y-m-d H:i:s', $available_from_date_start)."\n";
        echo $available_from_date_end . " - ".date('Y-m-d H:i:s', $available_from_date_end)."\n";
        return ExitCode::OK;
    }

    public function actionApplicprocessed()
    {
        $applic_processed=Info::findOne("applic_processed");
        if($applic_processed instanceof Info){
            $text=$applic_processed->text;
            return ExitCode::OK;
        }
        //if($applic_processed)
        

        return ExitCode::UNSPECIFIED_ERROR;
    }

    public function actionDelaypay()
    {

        $date=date('Y-m-d');

        $searchModel = new ApplicationsSearch();

        $available_from_date_start  = strtotime($date." +3 days");
        $queryParams=[
            'ApplicationsSearch'=>[
                'date_payment'=>date('Y-m-d', $available_from_date_start)
            ]
        ];
        
        $result=$searchModel->search_for_push($queryParams);

        if(count($result)>0){
            $applic_processed=Info::findOne("delay_pay_3day");
            if($applic_processed instanceof Info){
                $text=$applic_processed->text;
                foreach ($result as $value) {
                    if(!empty($value['push_id'])){
                        $body=str_replace("{sum}", ceil($value['sum']), $text);
                        $res=$this->SendPUSH($body, $value['push_id'], $data=['application_id'=>$value['id']]);
                    }
                }
                
            }
        }

        $available_from_date_start  = strtotime($date." +1 days");

        $queryParams=[
            'ApplicationsSearch'=>[
                'date_payment'=>date('Y-m-d', $available_from_date_start)
            ]
        ];
        
        $result=$searchModel->search_for_push($queryParams);

        if(count($result)>0){
            $applic_processed=Info::findOne("delay_pay_1day");
            if($applic_processed instanceof Info){
                $text=$applic_processed->text;
                foreach ($result as $value) {
                    if(!empty($value['push_id'])){
                        $body=str_replace("{sum}", ceil($value['sum']), $text);
                        $res=$this->SendPUSH($body, $value['push_id'], $data=['application_id'=>$value['id']]);
                    }
                }
                
            }
        }

        $available_from_date_start  = strtotime($date);

        $queryParams=[
            'ApplicationsSearch'=>[
                'date_payment'=>date('Y-m-d', $available_from_date_start)
            ]
        ];
        
        $result=$searchModel->search_for_push($queryParams);

        if(count($result)>0){
            $applic_processed=Info::findOne("day_pay");
            if($applic_processed instanceof Info){
                $text=$applic_processed->text;
                foreach ($result as $value) {
                    if(!empty($value['push_id'])){
                        $body=str_replace("{sum}", ceil($value['sum']), $text);
                        $res=$this->SendPUSH($body, $value['push_id'], $data=['application_id'=>$value['id']]);
                    }
                }
                
            }
        }

        //if($applic_processed)
        

        return ExitCode::UNSPECIFIED_ERROR;
    }

    private function SendPUSH($body, $to, $data=[]){
        $fields = array(
            'to'=> "ExponentPushToken[".$to."]",
            'sound'=>'default',
            'body'  => $body,
            'data' => $data
        );
         
        $headers = array
        (
            'Content-Type: application/json'
        );
         
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://exp.host/--/api/v2/push/send');
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        return $result;
    }
}
