<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%company}}`
 */
class m210414_094709_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(20),
            'access_token' => $this->string(255),
            'username' => $this->string(255),
            'auth_key' => $this->string(255),
            'password_hash' => $this->string(255),
            'password_reset_token' => $this->string(255),
            'created_at' => $this->integer(),
            'expire_at' => $this->integer(),
            'company_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `company_id`
        $this->createIndex(
            '{{%idx-users-company_id}}',
            '{{%users}}',
            'company_id'
        );

        // creates index for column `email`
        $this->createIndex(
            '{{%idx-users-email}}',
            '{{%users}}',
            'email'
        );
        // creates index for column `access_token`
        $this->createIndex(
            '{{%idx-users-access_token}}',
            '{{%users}}',
            'access_token'
        );
        // creates index for column `username`
        $this->createIndex(
            '{{%idx-users-username}}',
            '{{%users}}',
            'username'
        );
        // creates index for column `password_hash`
        $this->createIndex(
            '{{%idx-users-password_hash}}',
            '{{%users}}',
            'username'
        );
        // add foreign key for table `{{%company}}`
        $this->addForeignKey(
            '{{%fk-users-company_id}}',
            '{{%users}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE'
        );

        $this->insert ( '{{%users}}', 
            [
                'email' => 'dik-04@mail.ru',
                'access_token' => 'LVNhVG-WU4AItgx9heS7Htg7E54JfPuP',
                'username' => 'dik-04@mail.ru',
                'password_hash' => '$2y$13$pva9wFSXFlTsLVY07Vabf.l53xhJYnLDlBFaJoXaCjn5lu.PL7OoS',
                'company_id' => 1
            ]
         );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {   
        $this->delete('{{%users}}', ['id' => 1]);

        // drops foreign key for table `{{%company}}`
        $this->dropForeignKey(
            '{{%fk-users-company_id}}',
            '{{%users}}'
        );

        // drops index for column `company_id`
        $this->dropIndex(
            '{{%idx-users-company_id}}',
            '{{%users}}'
        );

        // drops index for column `email`
        $this->dropIndex(
            '{{%idx-users-email}}',
            '{{%users}}'
        );

        // drops index for column `access_token`
        $this->dropIndex(
            '{{%idx-users-access_token}}',
            '{{%users}}'
        );

        // drops index for column `username`
        $this->dropIndex(
            '{{%idx-users-username}}',
            '{{%users}}'
        );

        // drops index for column `password_hash`
        $this->dropIndex(
            '{{%idx-users-password_hash}}',
            '{{%users}}'
        );

        $this->dropTable('{{%users}}');
    }
}
