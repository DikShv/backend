<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%application_items}}`.
 */
class m210416_134952_add_specific_id_column_to_application_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // drops foreign key for table `{{%specifics}}`
        $this->dropForeignKey(
            '{{%fk-application_items-specific_id}}',
            '{{%application_items}}'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addForeignKey(
            '{{%fk-application_items-specific_id}}',
            '{{%application_items}}',
            'specific_id',
            '{{%specifics}}',
            'id',
            'CASCADE'
        );
    }
}
