<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%info}}`.
 */
class m210421_144403_create_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%info}}', [
            'key' => $this->string(20),
            'name' => $this->string(255),
            'text' => $this->text(),
        ]);

         $this->addPrimaryKey('info_pkey',
            '{{%info}}',
            'key'
        );

        $this->insert ( '{{%info}}', 
            [
                'key' => 'policy',
                'name' => 'Политика конфиденциальности',
                'text' => 'Текст политики'
            ]
        );
        $this->insert ( '{{%info}}', 
            [
                'key' => 'sms_reg',
                'name' => 'СМС с паролем',
                'text' => "Логин: {email}\nПароль: {password}\nПремиум сервис:"
            ]
        );

        $this->insert ( '{{%info}}', 
            [
                'key' => 'applic_processed',
                'name' => 'Заявка обработана',
                'text' => "Заявка обработана, доставка на {date} с 12 до 18"
            ]
        );

        $this->insert ( '{{%info}}', 
            [
                'key' => 'delay_pay_3day',
                'name' => 'За 3 дня до окончания отсрочки',
                'text' => "Добрый день, напоминаем Вам, что через 3 дня подходит срок оплаты: {sum}₽"
            ]
        );

        $this->insert ( '{{%info}}', 
            [
                'key' => 'delay_pay_1day',
                'name' => 'За 1 дня до окончания отсрочки',
                'text' => "Добрый день, напоминаем Вам, что завтра подходит срок оплаты: {sum}₽"
            ]
        );

        $this->insert ( '{{%info}}', 
            [
                'key' => 'day_pay',
                'name' => 'В день оплаты',
                'text' => "Добрый день, напоминаем Вам, что сегодня день оплаты: {sum}₽"
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%info}}', ['key' => 'policy']);
        $this->delete('{{%info}}', ['key' => 'sms_reg']);
        $this->delete('{{%info}}', ['key' => 'applic_processed']);
        $this->delete('{{%info}}', ['key' => 'delay_pay_3day']);
        $this->delete('{{%info}}', ['key' => 'delay_pay_1day']);
        $this->delete('{{%info}}', ['key' => 'day_pay']);
        $this->dropTable('{{%info}}');
        $this->dropPrimaryKey('info_pkey',
            '{{%info}}'
        );
    }
}
