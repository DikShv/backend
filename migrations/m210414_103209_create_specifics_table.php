<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%specifics}}`.
 */
class m210414_103209_create_specifics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%specifics}}', [
            'id' => $this->primaryKey(),
            'articul' => $this->string(20),
            'name' => $this->string(255),
            'measure' => $this->string(20),
            'price' => $this->money(),
            'stock_balance' => $this->integer(),
        ]);

        $this->insert ( '{{%specifics}}', 
            [
                'articul' => '698777',
                'name' => 'Искра ср-во для мытья стекол с курком 500 мл 1/15шт с Нашатырным спиртом',
                'measure' => '20 коробок',
                'price' => '200',
                'stock_balance' => '1000'
            ]
         );

        $this->insert ( '{{%specifics}}', 
            [
                'articul' => '54645646',
                'name' => 'Искра ср-во для мытья стекол с курком 500 мл 1/15шт с Нашатырным спиртом',
                'measure' => '20 коробок',
                'price' => '100',
                'stock_balance' => '10'
            ]
         );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%specifics}}', ['id' => 1]);
        $this->delete('{{%specifics}}', ['id' => 2]);
        $this->dropTable('{{%specifics}}');
    }
}
