<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%application_items}}`.
 */
class m210416_204640_add_sum_column_to_application_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%application_items}}', 'sum', $this->money()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%application_items}}', 'sum');
    }
}
