<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company}}`.
 */
class m210414_081518_create_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company}}', [
            'id' => 'SERIAL',
            'email' => 'character varying(20)',
            'name' => 'character varying(255)',
            'is_debt' => 'boolean'
        ]);

        $this->addPrimaryKey('company_pkey',
            '{{%company}}',
            'id'
        );

        $this->insert ( '{{%company}}', 
            [
                'email' => 'dik-04@mail.ru',
                'name' => 'Тестовая компания',
            ]
         );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%company}}', ['id' => 1]);
        
        $this->dropTable('{{%company}}');

        $this->dropPrimaryKey('company_pkey',
            '{{%company}}'
        );

    }
}
