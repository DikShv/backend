<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%application_items}}`.
 */
class m210416_134000_add_name_column_to_application_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%application_items}}', 'name', $this->string()->defaultValue(''));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%application_items}}', 'name');
    }
}
