<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%applications}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m210414_104012_create_applications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%applications}}', [
            'id' => $this->primaryKey(),
            'date_application' => $this->integer(),
            'date_shipping' => $this->integer(),
            'date_deferral' => $this->integer(),
            'day_deferral' => $this->integer(),
            'sum' => $this->money(),
            'status' => $this->integer(),
            'user_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-applications-user_id}}',
            '{{%applications}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-applications-user_id}}',
            '{{%applications}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        $this->insert ( '{{%applications}}', 
            [
                'date_application' => time(),
                'date_shipping' => time()+24*60,
                'date_deferral' => time()+7*24*60,
                'day_deferral' => '7',
                'sum' => '200',
                'status' => '1',
                'user_id' => '1',
            ]
        );

        $this->insert ( '{{%applications}}', 
            [
                'date_application' => time(),
                'date_shipping' => time()+24*60,
                'date_deferral' => time()+7*24*60,
                'day_deferral' => '7',
                'sum' => '200',
                'status' => '1',
                'user_id' => '1',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%applications}}', ['id' => 1]);
        $this->delete('{{%applications}}', ['id' => 2]);
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-applications-user_id}}',
            '{{%applications}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-applications-user_id}}',
            '{{%applications}}'
        );

        $this->dropTable('{{%applications}}');
    }
}
