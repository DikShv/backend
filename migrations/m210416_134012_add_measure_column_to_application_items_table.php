<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%application_items}}`.
 */
class m210416_134012_add_measure_column_to_application_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%application_items}}', 'measure', $this->string(20)->defaultValue(''));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%application_items}}', 'measure');
    }
}
