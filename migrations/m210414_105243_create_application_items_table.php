<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%application_items}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%specifics}}`
 * - `{{%applications}}`
 */
class m210414_105243_create_application_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%application_items}}', [
            'id' => $this->primaryKey(),
            'specific_id' => $this->integer()->notNull(),
            'application_id' => $this->integer()->notNull(),
            'count' => $this->integer(),
        ]);

        // creates index for column `specific_id`
        $this->createIndex(
            '{{%idx-application_items-specific_id}}',
            '{{%application_items}}',
            'specific_id'
        );

        // add foreign key for table `{{%specifics}}`
        $this->addForeignKey(
            '{{%fk-application_items-specific_id}}',
            '{{%application_items}}',
            'specific_id',
            '{{%specifics}}',
            'id',
            'CASCADE'
        );

        // creates index for column `application_id`
        $this->createIndex(
            '{{%idx-application_items-application_id}}',
            '{{%application_items}}',
            'application_id'
        );

        // add foreign key for table `{{%applications}}`
        $this->addForeignKey(
            '{{%fk-application_items-application_id}}',
            '{{%application_items}}',
            'application_id',
            '{{%applications}}',
            'id',
            'CASCADE'
        );

        $this->insert ( '{{%application_items}}', 
            [
                'specific_id' => '1',
                'application_id' => '1',
                'count' => '7',
            ]
        );

        $this->insert ( '{{%application_items}}', 
            [
                'specific_id' => '1',
                'application_id' => '1',
                'count' => '3',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%application_items}}', ['id' => 1]);
        $this->delete('{{%application_items}}', ['id' => 2]);
        
        // drops foreign key for table `{{%specifics}}`
        $this->dropForeignKey(
            '{{%fk-application_items-specific_id}}',
            '{{%application_items}}'
        );

        // drops index for column `specific_id`
        $this->dropIndex(
            '{{%idx-application_items-specific_id}}',
            '{{%application_items}}'
        );

        // drops foreign key for table `{{%applications}}`
        $this->dropForeignKey(
            '{{%fk-application_items-application_id}}',
            '{{%application_items}}'
        );

        // drops index for column `application_id`
        $this->dropIndex(
            '{{%idx-application_items-application_id}}',
            '{{%application_items}}'
        );

        $this->dropTable('{{%application_items}}');
    }
}
