<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%specifics}}`.
 */
class m210421_131044_add_photo_column_to_specifics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%specifics}}', 'photo', $this->string()->defaultValue(''));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%specifics}}', 'photo');
    }
}
