<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Company;
use app\modules\admin\models\CompanySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\admin\models\Users;
use app\modules\admin\models\Info;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'passwordsend'],
            'rules' => [
                [
                    'actions' => ['index', 'passwordsend'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPasswordsend($id)
    {
        $model = $this->findModel($id);

        if(is_array(Yii::$app->request->post('Company'))){
            $user=Users::findByCompany($id);
            if($user===null){
                $user = new Users();
            }

            $post=Yii::$app->request->post('Company');
            $password=Yii::$app->security->generateRandomString(8);

            $user->email=$model->email;
            $user->username=$post['email'];
            $user->company_id=$id;
            $user->setPassword($password);
            if($user->save()){
            	$sms_text=Info::findOne("sms_reg");
            	$sms_text=str_replace("{email}", $post['email'], $sms_text->text);
            	$sms_text=str_replace("{password}", $password, $sms_text);
                //$notice = "Логин: ".$post['email']."\nПароль: ".$password."\nПремиум сервис:";
                $msg = urlencode(iconv('utf-8', 'cp1251', $sms_text));
                $sms=file_get_contents("https://sms.ru/sms/send?api_id=589B62BF-E3E&to[".$post['phone']."]=".$msg."&json=1&translit=1");
                $sms=json_decode($sms, true);
                if($sms['status']=='OK'){
                    if($sms['sms'][$post['phone']]['status']=='OK'){
                        $model->password_issued=true;
                        $model->save();

                        return $this->redirect(['index']);
                    }
                }
            }
            
            //print_r(Yii::$app->request->post('Company')); exit;
        }
        /*if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['index']);
        }*/
        return $this->render('password_send', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new Company();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }*/

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }*/

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
