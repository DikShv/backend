<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use yii\filters\auth\HttpBearerAuth;

/**
 * Default controller for the `admin` module
 */
class AdminController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
        	'bearerAuth' => HttpBearerAuth::className(),
            'class' => HttpBearerAuth::className(),
            'except'=> ['login'],
        ];
        return $behaviors;
    }
}
