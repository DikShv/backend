<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пользователи');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
    <p>
        <?= Html::a(Yii::t('app', 'Пользователи'), ['/'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Компании'), ['/company'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Информация'), ['/info'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'id',
                'contentOptions' => ['style' => 'width:15px;  min-width:15px;  '],
            ],
            //'email:email',
            //'access_token',
            ['attribute' => 'username',
                'contentOptions' => ['style' => 'width:200px;  min-width:15px;  '],
            ],
            'is_auth:boolean',
            //'password_hash',
            //'password_reset_token',
            //'created_at',
            //'expire_at',
            ['attribute' => 'companyname',
                'contentOptions' => ['style' => 'width:800px;  min-width:600px;  '],
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
