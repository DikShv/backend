<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\InfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Информация');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-index">
    <p>
        <?= Html::a(Yii::t('app', 'Пользователи'), ['/'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Компании'), ['/company'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Информация'), ['/info'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'key',
            'name',
            'text:ntext',

            [
                'class' => \yii\grid\ActionColumn::class,

                'template' => '{update}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
