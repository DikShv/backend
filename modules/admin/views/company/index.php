<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Компании');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">
    <p>
        <?= Html::a(Yii::t('app', 'Пользователи'), ['/'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Компании'), ['/company'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Информация'), ['/info'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/

            ['attribute' => 'id',
                'contentOptions' => ['style' => 'width:15px;  min-width:15px;  '],
            ],
            'email:email',
            ['attribute' => 'name',
                'contentOptions' => ['style' => 'width:600px;  min-width:600px;  '],
            ],
            //'is_debt:boolean',
            /*'debt',
            'overdue_debt',
            'inn',*/
            'phone',
            'password_issued:boolean',
            [
                'class' => \yii\grid\ActionColumn::class,

                'template' => '{password_send}',

                'buttons' => [
                    'password_send' => function ($url, $model, $key) {
                        return Html::a(Yii::t('app', 'Выдать пароль'), ['/password_send?id='.$key], ['class' => 'btn btn-primary']);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
