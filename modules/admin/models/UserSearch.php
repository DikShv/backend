<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Users;
use app\modules\admin\models\Company;

/**
 * UserSearch represents the model behind the search form of `app\models\v1\Users`.
 */
class UserSearch extends Users
{
    public $companyname;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'expire_at', 'company_id'], 'integer'],
            [['email', 'access_token', 'username', 'auth_key', 'password_hash', 'password_reset_token', 'companyname'], 'safe'],
            [['is_auth'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = Users::find();

        $query=Users::find()->alias('u')
        ->leftJoin(Company::tableName().' c', 'c.id=u.company_id')
        ->select('u.*, c.name as companyname');
        /*->groupBy('a.id')
        ->asArray()
        ->with(['company']);*/
        //->where(['a.id'=>$id,'user_id'=>Yii::$app->user->id])->one();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'expire_at' => $this->expire_at,
            'company_id' => $this->company_id,
            'is_auth' => $this->is_auth,
        ]);



        $query->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'access_token', $this->access_token])
            ->andFilterWhere(['ilike', 'c.name', $this->companyname])
            ->andFilterWhere(['ilike', 'username', $this->username])
            ->andFilterWhere(['ilike', 'auth_key', $this->auth_key])
            ->andFilterWhere(['ilike', 'password_hash', $this->password_hash])
            ->andFilterWhere(['ilike', 'password_reset_token', $this->password_reset_token]);
        //print_r($query->createCommand()->getRawSql());exit();
        return $dataProvider;
    }
}
