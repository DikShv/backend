<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "info".
 *
 * @property string $key
 * @property string|null $name
 * @property string|null $text
 */
class Info extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['text'], 'string'],
            [['key'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'key' => 'Key',
            'name' => 'Название',
            'text' => 'Текст',
        ];
    }
}
