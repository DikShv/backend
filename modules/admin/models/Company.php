<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string|null $email
 * @property string|null $name
 * @property bool|null $is_debt
 *
 * @property Users[] $users
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['phone'], 'required'],
            [['is_debt'], 'boolean'],
            [['email'], 'string', 'max' => 20],
            [['name'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 12],
            [['password_issued'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
            'name' => 'Наименование компании',
            'is_debt' => 'Наличие задолженности',
            'debt' => 'Просроченная задолженность',
            'overdue_debt' => 'Задолженность',
            'inn' => 'ИНН',
            'phone' => 'Телефон',
            'password_issued' => 'Доступ выдан'
        ];
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['company_id' => 'id']);
    }
}
