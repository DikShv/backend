<?php

namespace app\controllers\v1;

class SpecificsController extends \yii\rest\ActiveController
{
	public $modelClass = 'app\models\v1\Specifics';

    public function actions(){
		$actions = parent::actions();
		
		unset($actions['delete'], $actions['create'], $actions['update'], $actions['options']);
		return $actions;
	}

}
