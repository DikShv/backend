<?php

namespace app\controllers\v1;

//use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use app\models\v1\LoginForm;
use app\models\v1\Company;
use app\models\v1\Applications;
use Yii;

class UsersController extends \yii\rest\ActiveController
{	
	public $modelClass = 'app\models\v1\Users';


	public function actions(){
		$actions = parent::actions();
		
		unset($actions['index'],$actions['view'], $actions['delete'], $actions['create'], $actions['update'], $actions['options']);
		$actions=[];
		return $actions;
	}

	public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'except'=> ['login'],
        ];
        return $behaviors;
    }

    public function actionIndex(){
        $user=Company::findOne(['id'=>Yii::$app->user->identity->getAttribute('company_id')])->getAttributes(['name', 'debt', 'overdue_debt', 'inn']);

        $aplication=Applications::find()->where(['user_id'=>Yii::$app->user->id])->count();
        $user['application_count']=$aplication;
        return $user;
    }

	public function actionLogin()
    {

        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            return [
                'access_token' => $model->login(),
            ];
        } else {
            return $model->getFirstErrors();
        }
    }

    public function actionTest()
    {
        return ['status'=>'success'];
    }
	
}
