<?php

namespace app\controllers\v1;

use yii\filters\auth\QueryParamAuth;
use app\models\v1\LoginForm;
use app\models\v1\Applications;
use app\models\v1\ApplicationItems;
use app\models\v1\Specifics;
use app\models\v1\ApplicationsSearch;
use Yii;

class ApplicationsController extends \yii\rest\ActiveController
{
	public $modelClass = 'app\models\v1\Applications';

    public function actions(){
		$actions = parent::actions();
		
		unset($actions['index'], $actions['view'], $actions['delete'], $actions['create'], $actions['update'], $actions['options']);
		//$actions=[];
		return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function () {
                    $searchModel = new ApplicationsSearch();
                    return $searchModel->search(\Yii::$app->request->queryParams);
                },
            ],
        ];
		//return $actions;
	}

	public function actionView($id){
		$aplication=Applications::find()->alias('a')
        ->leftJoin(ApplicationItems::tableName().' ai', 'a.id=ai.application_id')
        ->select('a.*, SUM(ai.sum) as sum')
        ->groupBy('a.id')
        ->asArray()
        ->with(['applicationItems'])
        ->where(['a.id'=>$id,'user_id'=>Yii::$app->user->id])->one();

        if(!is_null($aplication['date_application']))
            $aplication['date_application'] = Yii::$app->formatter->asDate($aplication['date_application']);
        if(!is_null($aplication['date_shipping']))
            $aplication['date_shipping'] = Yii::$app->formatter->asDate($aplication['date_shipping']);
        if(!is_null($aplication['date_deferral']))
            $aplication['date_deferral'] = Yii::$app->formatter->asDate($aplication['date_deferral']);
        if(!is_null($aplication['date_payment']))
            $aplication['date_payment'] = Yii::$app->formatter->asDate($aplication['date_payment']);

		return $aplication;
	}

    public function actionCreate(){
		$post=Yii::$app->request->post();
		$date=date('Y-m-d');
		$date_application = strtotime($post['date_application']);
		$H=date('H');
		$w=date('w');
		switch ($w) {
			case 0:
				$available_from_date  = strtotime($date." +2 days");
				break;
			case 6:
				$available_from_date  = strtotime($date." +3 days");
				break;
			default:
				if($H<17){
					$d=($w==5?3:1);
					$available_from_date  = strtotime($date." +".$d." days");
				}else{
					$d=($w==5?4:2);
					$available_from_date  = strtotime($date." +".$d." days");
				}
				break;
		}
		if($date_application<$available_from_date){
			Yii::$app->response->setStatusCode(422);
        	return ['status'=>'error_date_application'];
		}

        $Applications = new Applications();
        $sum=0;
        $post=Yii::$app->request->post();
        $Applications_post['Applications']=[];
        $Applications_post['Applications']['date_application']=$post['date_application'];
        $Applications_post['Applications']['comment']=$post['comment'];
        $post['items']=[];
        foreach ($post['products'] as $product) {
            $post['items'][$product["product_id"]]=$product["count"];
        }
        if($Applications->load($Applications_post) && $Applications->validate() && count($post['items'])>0){
            
            $specific=array_keys($post['items']);
            $Specifics=Specifics::findAll(['id' => $specific]);
            if(count($Specifics)>0){
                $Applications->user_id=Yii::$app->user->id;
                $Applications->status=1;
                if($Applications->save()){
                    foreach ($Specifics as $specific) {
                        $count=intval($post['items'][$specific->id]);
                        if($specific->stock_balance>$count){
                            $ApplicationItems = new ApplicationItems();
                            $ApplicationItems->application_id=$Applications->id;
                            $ApplicationItems->specific_id=$specific->id;
                            $ApplicationItems->count=$count;
                            $ApplicationItems->name=$specific->name;
                            $ApplicationItems->measure=$specific->measure;
                            $ApplicationItems->price=$specific->price;
                            $ApplicationItems->sum=$count*$specific->price;
                            $ApplicationItems->save();
                        }else{
                            Yii::$app->response->setStatusCode(422);
                            return ['status'=>'failure'];
                        }
                        
                    }
                    return ['status'=>'success'];
                } 
            }
            
        }
        Yii::$app->response->setStatusCode(422);
        return ['status'=>'failure'];
    }

	public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        
        return $behaviors;
    }



}
