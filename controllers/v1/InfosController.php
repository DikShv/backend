<?php

namespace app\controllers\v1;

class InfosController extends \yii\rest\ActiveController
{
	public $modelClass = 'app\models\v1\Infos';

    public function actions(){
		$actions = parent::actions();
		
		unset($actions['index'], $actions['delete'], $actions['create'], $actions['update'], $actions['options']);
		return $actions;
	}

	/*public function actionView($id)
    {
        return Info::findOne($id);
    }*/

    

}
